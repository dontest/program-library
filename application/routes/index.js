'use strict'

const 
    apiRoute = require('./api')

const init = app => {
    app.use('/api', apiRoute)
}

module.exports = {init}