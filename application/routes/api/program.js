'use strict'

const
    express = require('express'),
    router = express.Router(),
    controller = require('../../controllers/api/program')

router.get('/', controller.getPrograms)

module.exports = router