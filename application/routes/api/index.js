'use strict'

const 
    express = require('express'),
    router = express.Router(),
    programRoute = require('./program')

router.use('/programs', programRoute)

module.exports = router