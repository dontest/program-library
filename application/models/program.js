'use strict'

const 
    db = require('../config/database'),
    _ = require('lodash')

const getPrograms = async () => {
    const
        conn = await db.getDBConnection(),
        sql = `SELECT Program.id programId, Program.name programName, Program.description programDescription,
            Section.id sectionId, Section.name sectionName, Section.description sectionDescription, Section.image sectionImage,
            Section.orderIndex sectionOrderIndex, Activity.id activityId, Activity.type activityType, Activity.title activityTitle,
            Activity.orderIndex activityOrderIndex, ActivityOption.id activityOptionId, 
            ActivityOption.value activityOptionValue, ActivityOption.orderIndex activityOptionOrderIndex
            FROM Program
            LEFT JOIN Section ON Program.id = Section.programId
            LEFT JOIN Activity ON Section.id = Activity.sectionId
            LEFT JOIN ActivityOption ON Activity.id = ActivityOption.activityId
            WHERE Activity.isActive = 1 AND ActivityOption.isActive = 1`

    const result = await conn.query(sql)
    const programs = await groupBy(result[0], ['programId', 'sectionId', 'activityId'])

    return programs
}

async function groupBy(arr, keys) {
    let key = keys[0]

    if (!key) return arr  
                
    let list = Object.values(
        arr.reduce((obj, current) => {
            let 
                o,
                program = {
                    programId: current.programId,
                    programName: current.programName,
                    programDescription: current.programDescription
                },
                section = {
                    sectionId: current.sectionId,
                    sectionName: current.sectionName,
                    sectionDescription: current.sectionDescription,
                    sectionImage: current.sectionImage,
                    sectionOrderIndex: current.sectionOrderIndex
                },
                activity = {
                    activityId: current.activityId,
                    activityType: current.activityType,
                    activityTitle: current.activityTitle,
                    activityOrderIndex: current.activityOrderIndex
                }

            switch (key) {
                case 'programId': 
                    o = program
                    break
                case 'sectionId': 
                    o = section
                    break
                case 'activityId': 
                    o = activity
                    break
            }

            let rest = _.omit(current, Object.keys(o));

            if (!obj[current[key]]) obj[current[key]] = {...o, list: []}

            obj[current[key]].list.push(rest)

            return obj
        }, {}))

    if (keys.length){
        list.forEach(async obj => {
            obj.count = obj.list.length
            obj.list = await groupBy(obj.list, keys.slice(1))
        })
    }

    return list
}

module.exports = {
    getPrograms
}