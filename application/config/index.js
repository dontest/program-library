'use strict'

module.exports = {
    dbConfig: {
        database: process.env.DATABASE,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        debug: process.env.DB_DEBUG
    }
}