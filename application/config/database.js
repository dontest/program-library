'use strict'

const 
    config = require('../config'),
    {Sequelize} = require('sequelize')

const getDBConnection = async () => {
    const sequelize = new Sequelize(config.dbConfig.database, config.dbConfig.user, config.dbConfig.password, {
        host: config.dbConfig.host,
        port: config.dbConfig.port,
        dialect: 'mysql',
        timezone: '-05:00',
        pool: {
            max: 15,
            min: 0
        }
    })

    return sequelize
}

module.exports = {getDBConnection}