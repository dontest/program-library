'use strict'

const
    programModel = require('../../models/program')

const getPrograms = async (req, res) => {
    try {
        const result = await programModel.getPrograms()
        return res.status(200).json({success: true, data: result})
    }
    catch(e) {
        return res.status(500).json({success: false})
    }
}

module.exports = {
    getPrograms
}