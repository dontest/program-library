drop database if exists ProgramLibrary;

CREATE DATABASE ProgramLibrary;
USE ProgramLibrary;

CREATE TABLE Program (
    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(255),
    createDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
    updateDate DATETIME NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE Section (
    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    programId INT(11) UNSIGNED NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(255),
    image VARCHAR(255),
    orderIndex INT(11) UNSIGNED NOT NULL,
    createDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updateDate DATETIME NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (programId) REFERENCES Program(id),
    KEY idxSectionProgramId (programId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE Activity (
    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    sectionId INT(11) UNSIGNED NOT NULL,
    type VARCHAR(50) NOT NULL,
    title VARCHAR(255) NOT NULL,
    isActive BOOLEAN NOT NULL DEFAULT 1,
    orderIndex INT(11) UNSIGNED NOT NULL,
    createDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updateDate DATETIME NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (sectionId) REFERENCES Section(id),
    KEY idxActivitySectionId (sectionId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE ActivityOption (
    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    activityId INT(11) UNSIGNED NOT NULL,
    value VARCHAR(255) NOT NULL,
    isActive BOOLEAN NOT NULL DEFAULT 1,
    orderIndex INT(11) UNSIGNED NOT NULL,
    createDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updateDate DATETIME NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (activityId) REFERENCES Activity(id),
    KEY idxActivityOptionActivityId (activityId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO Program(name, description) VALUES ('Leadership Development Program', 'This is Leadership Development Program Description');
INSERT INTO Program(name, description) VALUES ('Cognitive Behavioral Therapy', 'This is Cognitive Behavioral Therapy Description');
INSERT INTO Program(name, description) VALUES ('New Parenting', 'This is New Parenting Description');
INSERT INTO Program(name, description) VALUES ('Mindful Communication', 'This is Mindful Communication Description');

INSERT INTO Section(programId, name, description, image, orderIndex) VALUES 
(1, 'Part One', 'Leadership Development Program Section 1', '/Leadership1.png', 1),
(1, 'Part Two', 'Leadership Development Program Section 2', '/Leadership2.png', 2),
(1, 'Part Three', 'Leadership Development Program Section 3', '/Leadership3.png', 3),
(1, 'Part Four', 'Leadership Development Program Section 4', '/Leadership4.png', 4),
(1, 'Part Five', 'Leadership Development Program Section 5', '/Leadership5.png', 5),
(1, 'Part Six', 'Leadership Development Program Section 6', '/Leadership6.png', 6),
(1, 'Part Seven', 'Leadership Development Program Section 7', '/Leadership7.png', 7),
(1, 'Part Eight', 'Leadership Development Program Section 8', '/Leadership8.png', 8),
(1, 'Part Nine', 'Leadership Development Program Section 9', '/Leadership9.png', 9),
(1, 'Part Ten', 'Leadership Development Program Section 10', '/Leadership10.png', 10),
(2, 'Part One', 'Cognitive Behavioral Therapy Section 1', '/Cognitive1.png', 1),
(2, 'Part Two', 'Cognitive Behavioral Therapy Section 2', '/Cognitive2.png', 2),
(2, 'Part Three', 'Cognitive Behavioral Therapy Section 3', '/Cognitive3.png', 3),
(2, 'Part Four', 'Cognitive Behavioral Therapy Section 4', '/Cognitive4.png', 4),
(2, 'Part Five', 'Cognitive Behavioral Therapy Section 5', '/Cognitive5.png', 5),
(2, 'Part Six', 'Cognitive Behavioral Therapy Section 6', '/Cognitive6.png', 6),
(2, 'Part Seven', 'Cognitive Behavioral Therapy Section 7', '/Cognitive7.png', 7),
(2, 'Part Eight', 'Cognitive Behavioral Therapy Section 8', '/Cognitive8.png', 8),
(3, 'Part One', 'New Parenting Section 1', '/New1.png', 1),
(3, 'Part Two', 'New Parenting Section 2', '/New2.png', 2),
(3, 'Part Three', 'New Parenting Section 3', '/New3.png', 3),
(3, 'Part Four', 'New Parenting Section 4', '/New4.png', 4),
(4, 'Part One', 'Mindful Communication Section 1', '/Mindful1.png', 1),
(4, 'Part Two', 'Mindful Communication Section 2', '/Mindful2.png', 2),
(4, 'Part Three', 'Mindful Communication Section 3', '/Mindful3.png', 3),
(4, 'Part Four', 'Mindful Communication Section 4', '/Mindful4.png', 4);

INSERT INTO Activity(sectionId, type, title, orderIndex) VALUES 
(1, 'text', 'What Does Leadership Development Program Do?', 1),
(1, 'checkbox', 'How do you want to use Leadership Development Program to improve your work?', 2),
(11, 'text', 'What Does Cognitive Behavioral Therapy Do?', 1),
(11, 'checkbox', 'How do you want to use Cognitive Behavioral Therapy to improve your work?', 2),
(19, 'text', 'What Does New Parenting Do?', 1),
(19, 'checkbox', 'How do you want to use New Parenting to improve your work?', 2),
(23, 'text', 'What Does Mindful Communication Do?', 1),
(23, 'checkbox', 'How do you want to use Mindful Communication to improve your work?', 2);

INSERT INTO ActivityOption(activityId, value, orderIndex) VALUES 
(1, 'Better self-control', 1),
(1, 'Improved mental clarify', 2),
(1, 'Reduced stress', 3),
(1, 'Better sleep', 4),
(1, 'Increase self-awareness', 5),
(1, 'More empathy', 6),
(2, 'Increase focus', 1),
(2, 'Improve Concentration', 2),
(2, 'Mental Clarify', 3),
(2, 'Reduce Stress', 4),
(2, 'Respond With Kindness', 5);

