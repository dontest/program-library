I. Instruction to use the app
- Install dependent modules   
    1. cd program-library
    2. npm install

- Modify program-library/.env file to update database config

- Import library/schema.sql to create database, tables, insert data

- Run the application
    1. npm start

- API Endpoint: GET http://localhost:3000/api/programs

- API json Response: 
{
    success: true,
    data: [
        {
        programId: 1,
        programName: "Leadership Development Program",
        programDescription: "This is Leadership Development Program Description",
        list: [
        {
        sectionId: 1,
        sectionName: "Part One",
        sectionDescription: "Leadership Development Program Section 1",
        sectionImage: "/Leadership1.png",
        sectionOrderIndex: 1,
        list: [
        {
        activityId: 1,
        activityType: "text",
        activityTitle: "What Does Leadership Development Program Do?",
        activityOrderIndex: 1,
        list: [
        {
        activityOptionId: 1,
        activityOptionValue: "Better self-control",
        activityOptionOrderIndex: 1
        },
        {
        activityOptionId: 2,
        activityOptionValue: "Improved mental clarify",
        activityOptionOrderIndex: 2
        },
        {
        activityOptionId: 3,
        activityOptionValue: "Reduced stress",
        activityOptionOrderIndex: 3
        },
        {
        activityOptionId: 4,
        activityOptionValue: "Better sleep",
        activityOptionOrderIndex: 4
        },
        {
        activityOptionId: 5,
        activityOptionValue: "Increase self-awareness",
        activityOptionOrderIndex: 5
        },
        {
        activityOptionId: 6,
        activityOptionValue: "More empathy",
        activityOptionOrderIndex: 6
        }
        ],
        count: 6
        },
        {
        activityId: 2,
        activityType: "checkbox",
        activityTitle: "How do you want to use Leadership Development Program to improve your work?",
        activityOrderIndex: 2,
        list: [
        {
        activityOptionId: 7,
        activityOptionValue: "Increase focus",
        activityOptionOrderIndex: 1
        },
        {
        activityOptionId: 8,
        activityOptionValue: "Improve Concentration",
        activityOptionOrderIndex: 2
        },
        {
        activityOptionId: 9,
        activityOptionValue: "Mental Clarify",
        activityOptionOrderIndex: 3
        },
        {
        activityOptionId: 10,
        activityOptionValue: "Reduce Stress",
        activityOptionOrderIndex: 4
        },
        {
        activityOptionId: 11,
        activityOptionValue: "Respond With Kindness",
        activityOptionOrderIndex: 5
        }
        ],
        count: 5
        }
        ],
        count: 11
        }
        ],
        count: 11
        }
    ]
}

II. Unit test
1. cd program-library
2. npm test
