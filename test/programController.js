'use strict'

const 
    expect = require('chai').expect,
    sinon = require('sinon'),
    httpMocks = require('node-mocks-http'),
    programController = require('../application/controllers/api/program'),
    programModel = require('../application/models/program')

describe('Get Programs API',  () => {
    const programStubRes = {success: true, data: []}

    let 
        programStub,
        response = httpMocks.createResponse()

    describe('Correct data->', async () => { 
        const 
            request = {} 

        beforeEach(() => {
            programStub = sinon.stub(programModel, 'getPrograms').resolves([])
        })

        afterEach(() => {
            programStub.restore()
        })

        it('Should return status 200 and expected result', async () => {
            const res = await programController.getPrograms(request, response)
            expect(response.statusCode).to.eql(200)
            expect(response._getData()).to.eql(JSON.stringify(programStubRes))
        })
    })

    describe('Throw error->', async () => {
        const request = {}
        
        beforeEach(() => {
            programStub = sinon.stub(programModel, 'getPrograms').rejects()
        })

        afterEach(() => {
            programStub.restore()
        })

        it('Should return status 500', async () => {
            const res = await programController.getPrograms(request, response)
            expect(response.statusCode).to.eql(500)
        })      
    })
})


